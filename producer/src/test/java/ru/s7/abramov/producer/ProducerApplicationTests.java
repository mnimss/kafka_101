package ru.s7.abramov.producer;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.s7.abramov.producer.dto.Message;
import ru.s7.abramov.producer.service.MessageServiceImpl;

import java.util.UUID;

@SpringBootTest
class ProducerApplicationTests {

	@Autowired
	private MessageServiceImpl messageService;

	@Test
	void contextLoads() {
	}

	@Test
	void send() {
		for (int i = 0; i < 100; i++) {
			messageService.send(UUID.randomUUID(), new Message(String.valueOf(i), i, 1000 + i));
		}
	}
}
