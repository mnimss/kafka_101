package ru.s7.abramov.producer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.s7.abramov.producer.dto.Message;

import java.util.UUID;

@Service
public class MessageServiceImpl implements MessageService{

    private KafkaTemplate<UUID, Message> kafkaTemplate;

    @Value("${kafka.topic.name}")
    private String topic;

    @Autowired
    public MessageServiceImpl(KafkaTemplate<UUID, Message> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public void send(UUID id, Message message) {
        kafkaTemplate.send(topic, id, message);
    }
}
