package ru.s7.abramov.producer.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.s7.abramov.producer.dto.Message;
import ru.s7.abramov.producer.service.MessageService;

import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/test")
public class MessageController {

    private final MessageService messageService;

    @PutMapping("/{id}")
    public ResponseEntity<String> sendMessage(@PathVariable(name = "id") UUID id,
                                              @RequestBody Message message) {
        try {
            messageService.send(id, message);
            return ResponseEntity.ok("Message received: " + message.toString());

        } catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_GATEWAY)
                    .body("Error Message");
        }
    }
}
