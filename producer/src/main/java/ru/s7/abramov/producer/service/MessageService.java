package ru.s7.abramov.producer.service;

import ru.s7.abramov.producer.dto.Message;

import java.util.UUID;

public interface MessageService {

    void send(UUID id, Message message);
}
