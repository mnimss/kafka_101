package ru.s7.abramov.consumer.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;
import ru.s7.abramov.consumer.dto.EditMessage;
import ru.s7.abramov.consumer.dto.Message;

import java.util.UUID;

@Service
@Slf4j
public class MessageService {

    @KafkaListener(topics = {"${kafka.topic.name}"}, containerFactory = "kafkaListenerContainerFactory")
    public void consume(@Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) UUID id, Message message) {
        EditMessage editMessage = convertMessage(id, message);
        log.debug("{}", editMessage);
    }

    private EditMessage convertMessage(UUID id, Message message) {
        EditMessage editMessage = new EditMessage();
        editMessage.setId(id);
        editMessage.setName(message.getName());
        editMessage.setMultipliedSum(String.valueOf(message.getSum() * message.getMultiplier()));

        return editMessage;
    }
}
