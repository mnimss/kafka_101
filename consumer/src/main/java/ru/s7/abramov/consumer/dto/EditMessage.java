package ru.s7.abramov.consumer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditMessage {

    private UUID id;
    private String name;
    private String multipliedSum;
}
