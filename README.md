Технические требования:
- Java 11
- Spring Boot 2.2.4
- Kafka 2.3
- slf4j 1.7.30
- docker 19.03
- gradle 6.0.1

Команды запуска:
1. docker-compose up -d
2. cd producer
3. gradle clean build run
4. cd ..
5. cd consumer
6. gradle clean build run
